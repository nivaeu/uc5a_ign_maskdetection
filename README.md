# Mask Detection

## Introduction
This sub-project is part of the ["New IACS Vision in Action”
--- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for
e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate
data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under
grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on
[gitlab](https://gitlab.com/nivaeu/)

## Odeon Lancover

[ODEON](https://dai-projets.gitlab.io/odeon-landcover/index.html) stands for Object Delineation on Earth Observations with Neural network. It is a set of command-line tools performing semantic segmentation on remote sensing images (aerial and/or satellite) with as many layers as you wish.

## Odeon Installation

Install version 0.1 from [ODEON gitlab](https://gitlab.com/dai-projets/odeon-landcover) by downloading [odeon-landcover-0.1.tar.gz](https://gitlab.com/dai-projets/odeon-landcover/-/archive/0.1/odeon-landcover-0.1.tar.gz).

These instruction assume that you use a Linux or MacOS system and that you already have [conda](https://conda.io/) installed.


```shell
tar -xzvf odeon-landcover-0.1.tar.gz
cd odeon-landcover-0.1
conda env create --file=environment.yml
conda activate odeon
pip install .
```

Odeon is now ready to used
```shell
odeon
```

## Models

In this project, two models are used:
- *model/unet_building.pth*: a unet model based on 5 channels (R, G, B, IR, DSM-DTM), trained on 50cm images on different french areas (Gironde, Isère, Val d'Oise, Haute Loire, Vendée)
- *model/unet_vegtation.pth*: a unet model based on 5 channels (R, G, B, IR, DSM-DTM), trained on 20cm images on only one french area (Vendée)

## Dataset preparation

Detection is based on:
- a Color Ortho Image
- an IR Image
- a DSM (Digital Surface Model)
- a DTM (Digital Terrain Model)

All images have to be a the small resolution, these resolutions should be "more or less" the resolution used for model training.

This repository include 3 "ready to used" datasets in folders:
- data/test1
- data/test2
- data/test3

## Detection

For each dataset there is two configuration files: one for building detection (*param_bati.json*) and one for vegetation (*param_veget.json*).
A complete documentation about detection configuration is available [here](https://dai-projets.gitlab.io/odeon-landcover/detect.html).

```shell
mkdir building
odeon detect -c param_building.json
mkdir vegetation
odeon detect -c param_vegetation.json
```
