#
# Copyright (c) IGN 2019 -- 2021.
# This file belongs to subproject uc5a_ign_maskdetection of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
cd data/test1
mkdir building
odeon detect -c param_building.json
mkdir vegetation
odeon detect -c param_vegetation.json
cd ../..
cd data/test2
mkdir building
odeon detect -c param_building.json
mkdir vegetation
odeon detect -c param_vegetation.json
cd ../..
cd data/test3
mkdir building
odeon detect -c param_building.json
mkdir vegetation
odeon detect -c param_vegetation.json
 
